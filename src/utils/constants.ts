export class Constants {
    static SUCCESS_CODE = 0;
    static SUCCESS_MESSAGE = "Successful";

    static FAIL_CODE = 1;
    static FAIL_MESSAGE = "Failed";

    static SALT_OR_ROUNDS = 10;

    static FAIL_CHECK = {
        isSuccess: false,
        data: null
    }

    static LIST_COMMAND = {
        CREATE_NEW: "cr"
    }

    static ROOT_PATH = "/";
}
