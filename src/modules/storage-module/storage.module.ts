import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StorageEntity } from './entities/storage.entity';
import { StorageRepository } from './storage.repository';
import { StorageService } from './storage.service';
import { StorageSubscribe } from './storage.subscribe';


@Module({
    imports: [
        TypeOrmModule.forFeature([StorageEntity])
    ],
    controllers: [],
    providers: [StorageService, StorageRepository, StorageSubscribe],
    exports: [StorageService, StorageRepository]
})
export class StorageModule { }
