import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { StorageTypeEnum } from 'src/interfaces/storage.enum';
import { Constants } from 'src/utils/constants';
import { Repository } from 'typeorm';
import { GetListFileDTO } from '../command-module/dto/get_list_file.dto';
import { RemoveStorageDTO } from '../command-module/dto/remove_storage.dto';
import { StorageEntity } from './entities/storage.entity';


@Injectable()
export class StorageRepository {
    constructor(
        @InjectRepository(StorageEntity)
        private readonly storageRepository: Repository<StorageEntity>,
    ) { }

    public async save(storageEntity: StorageEntity): Promise<StorageEntity> {
        try {
            const result = await this.storageRepository.save(storageEntity);
            if (result) return result;
        } catch (e) {
            console.log("error");
        }
        return null;
    }

    public async saveListEntity(arrStorageEntity: StorageEntity[]): Promise<void> {
        try {
            console.time('insert')
            await this.storageRepository.save(arrStorageEntity);
            console.timeEnd('insert')
        } catch (e) {
            console.log("error");
        }
    }

    public async findFolderWithPath(path: string, userId: number, isOnlyFolder = true): Promise<StorageEntity> {
        const typeArr = [StorageTypeEnum.FOLDER];
        if(!isOnlyFolder){
            typeArr.push(StorageTypeEnum.FILE)
        }
        return await this.storageRepository.createQueryBuilder("storage")
            .where("storage.ownerId = :userId", { userId })
            .andWhere("storage.type IN (:...typeArr)", { typeArr })
            .andWhere("storage.path = :path", { path })
            .getOne();
    }



    public async getListStorage(data: GetListFileDTO, userId: number): Promise<StorageEntity[]> {
        let regexString = data.path === Constants.ROOT_PATH ? "^\/[^\/]*" : (data.path + "\/?[^\/]*?");
        regexString += "\/?[^\/]*?".repeat(data.level - 1) + "$";
        return await this.storageRepository.createQueryBuilder("storage")
            .where("storage.ownerId = :userId", { userId })
            .andWhere("REGEXP_LIKE(storage.path, :regexStr)", { regexStr: regexString })
            .getMany();
    }

    public async updateStorage(pathFolder: string, size: number, userId: number): Promise<void> {
        const pathArr = pathFolder.split("/");
        const arrPath = new Array(pathArr.length).fill(0).map((e, i) => pathArr.slice(0, i + 1).join("/")).map(e => e ? e : Constants.ROOT_PATH)
        await this.storageRepository.createQueryBuilder("storage")
            .update(StorageEntity)
            .set({
                size: () => `size + ${size}`
            })
            .where("storage.ownerId = :userId", { userId })
            .andWhere("storage.path IN (:...arrPath)", { arrPath })
            .execute();
    }

    public async _removeFolder(path: string, userId: number): Promise<boolean> {
        try {
            let regexString = path.replace(/\*/g, "[^/]*") + ".*$";
            await this.storageRepository.createQueryBuilder("storage")
                .delete()
                .from(StorageEntity)
                .andWhere("REGEXP_LIKE(storage.path, :regexStr)", { regexStr: regexString })
                .andWhere("storage.type = :type", { type: StorageTypeEnum.FOLDER })
                .andWhere("storage.path <> :path", { path: Constants.ROOT_PATH })
                .execute();
            return true;
        } catch (e) {
            console.log("error");
        }
        return false;
    }

    public async removeStorage(data: RemoveStorageDTO, userId: number): Promise<boolean> {
        try {
            let regexString = data.path.replace(/\*/g, "[^/]*") + (data.isRecursive ? ".*" : "") + "$";
            const listFiles = await this.storageRepository.createQueryBuilder("storage")
                .where("storage.ownerId = :userId", { userId })
                .andWhere("storage.type = :type", { type: StorageTypeEnum.FILE })
                .andWhere("REGEXP_LIKE(storage.path, :regexStr)", { regexStr: regexString })
                .getMany();
            if (listFiles.length) {
                await this.storageRepository.createQueryBuilder("storage")
                    .delete()
                    .from(StorageEntity)
                    .where("storage.id IN (:...ids)", { ids: listFiles.map(e => e.id) })
                    .execute();
            }
            if (data.isRecursive) { // Delete folder
                await this._removeFolder(data.path, userId);
            }
            // Decrease size
            listFiles.forEach((e) => {
                const arrPath = e.path.split("/");
                const pathFolder = arrPath.slice(0, arrPath.length - 1).join("/");
                this.updateStorage(pathFolder, -e.size, userId);
            })
            return true;

        } catch (e) {
            console.log("error");
        }
        return false;
    }

    public async getListStorageWithPath(path: string, userId: number): Promise<StorageEntity[]> {
        let regexString = path.replace(/\*/g, "[^/]*") + ".*$";
        return await this.storageRepository.createQueryBuilder("storage")
            .where("storage.ownerId = :userId", { userId })
            .andWhere("REGEXP_LIKE(storage.path, :regexStr)", { regexStr: regexString })
            .getMany();
    }
}
