import { StorageTypeEnum } from 'src/interfaces/storage.enum';
import {
    Connection,
    EntitySubscriberInterface,
    EventSubscriber,
    InsertEvent,
    RemoveEvent,
    UpdateEvent,
} from 'typeorm';
import { StorageEntity } from './entities/storage.entity';
import { StorageRepository } from './storage.repository';

@EventSubscriber()
export class StorageSubscribe implements EntitySubscriberInterface<StorageEntity> {
    constructor(
        connection: Connection,
        private readonly storageRepository: StorageRepository
    ) {
        connection.subscribers.push(this);
    }

    listenTo() {
        return StorageEntity;
    }

    beforeInsert(event: InsertEvent<StorageEntity>) {
        event.entity.created_at = new Date();
        event.entity.updated_at = new Date();
        event.entity.size = event.entity.data?.length || 0;
        event.entity.type = event.entity.data === null ? StorageTypeEnum.FOLDER : StorageTypeEnum.FILE;
    }

    async afterInsert(event: InsertEvent<StorageEntity>) {
        if (event.entity.type === StorageTypeEnum.FILE) {
            // update size of parent folder
            const arrPath = event.entity.path.split("/");
            const pathFolder = arrPath.slice(0, arrPath.length - 1).join("/");
            this.storageRepository.updateStorage(pathFolder, event.entity.size, event.entity.owner.id)
        }
    }

    beforeUpdate(event: UpdateEvent<StorageEntity>) {
        event.entity.updated_at = new Date();
    }
}
