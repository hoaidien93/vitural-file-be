import { Injectable } from '@nestjs/common';
import { Checker } from 'src/interfaces/checker.interface';
import { StorageTypeEnum } from 'src/interfaces/storage.enum';
import { Constants } from 'src/utils/constants';
import { UserEntity } from '../user-module/entites/user.entity';
import { StorageEntity } from './entities/storage.entity';
import { StorageRepository } from './storage.repository';


@Injectable()
export class StorageService {
    constructor(
        private readonly storageRepository: StorageRepository,
    ) { }


    public async createNew(fileName: string, data: string, folderRoot: StorageEntity, userId: number): Promise<Checker> {
        const storageEntity = new StorageEntity();
        storageEntity.data = data;
        storageEntity.name = fileName;
        storageEntity.path = (folderRoot.path === Constants.ROOT_PATH ? folderRoot.path : folderRoot.path + "/") + fileName;
        const userEntity = new UserEntity();
        userEntity.id = userId;
        storageEntity.owner = userEntity;
        const result = await this.storageRepository.save(storageEntity);
        if (result) {
            return {
                isSuccess: true,
                data: result
            }
        }
        return Constants.FAIL_CHECK;
    }

    public async createFolderRecursive(path: string, userId: number): Promise<void> {
        const [root, ...pathArr] = path.split("/");
        let parentFolder = await this.storageRepository.findFolderWithPath(Constants.ROOT_PATH, userId);
        let pathFolder = parentFolder.path;
        const listFolderEntity = [];
        const userEntity = new UserEntity();
        userEntity.id = userId;
        if (parentFolder) {
            for (let i = 0; i < pathArr.length; i++) {
                if (pathFolder === Constants.ROOT_PATH) {
                    pathFolder += pathArr[i]
                } else {
                    pathFolder += "/" + pathArr[i];
                }
                if (parentFolder) {
                    parentFolder = await this.storageRepository.findFolderWithPath(pathFolder, userId);
                }
                if (!parentFolder) {
                    const entity = new StorageEntity();
                    entity.owner = userEntity;
                    entity.path = pathFolder;
                    entity.name = pathArr[i];
                    entity.data = null;
                    entity.type = StorageTypeEnum.FOLDER;
                    listFolderEntity.push(entity)
                }

            }
        }
        await this.storageRepository.saveListEntity(listFolderEntity);
    }
}
