import { StorageTypeEnum } from 'src/interfaces/storage.enum';
import { UserEntity } from 'src/modules/user-module/entites/user.entity';
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity({ name: 'storage' })
@Unique("unq_storage_owner_path",["owner.id", "path"])
export class StorageEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: "longtext",
        default: null
    })
    data: string;

    @Column()
    created_at: Date

    @Column()
    updated_at: Date

    @Column({
        default: 0
    })// Size: Byte
    size: number;

    @ManyToOne(type => UserEntity, user => user.listStorage)
    owner: UserEntity

    @Column()
    path: string;

    @Column()
    name: string;

    @Column()
    type: StorageTypeEnum
}
