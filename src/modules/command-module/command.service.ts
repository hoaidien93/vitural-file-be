import { Injectable } from '@nestjs/common';
import { Checker } from 'src/interfaces/checker.interface';
import { StorageTypeEnum } from 'src/interfaces/storage.enum';
import { Constants } from 'src/utils/constants';
import { StorageEntity } from '../storage-module/entities/storage.entity';
import { StorageRepository } from '../storage-module/storage.repository';
import { StorageService } from '../storage-module/storage.service';
import { CreateFileDTO } from './dto/create_file.dto';
import { GetListFileDTO } from './dto/get_list_file.dto';
import { MoveStorage } from './dto/move_storage';
import { RemoveStorageDTO } from './dto/remove_storage.dto';

@Injectable()
export class CommandService {
    constructor(
        private readonly storageService: StorageService,
        private readonly storageRepository: StorageRepository,

    ) {

    }

    async createNewFile(data: CreateFileDTO, userId: number): Promise<Checker> {
        const pathArr = data.path.split("/");
        const parentPath = pathArr.slice(0, pathArr.length - 1).join("/");
        if (data.isCreate) {
            await this.storageService.createFolderRecursive(parentPath, userId);
        }
        const fileName = pathArr[pathArr.length - 1];
        const folder = await this.storageRepository.findFolderWithPath(parentPath || "/", userId);
        if (folder) {
            return await this.storageService.createNew(fileName, data.data, folder, userId);
        }
        return Constants.FAIL_CHECK;
    }

    async getListFile(data: GetListFileDTO, userId: number): Promise<Checker> {
        let listStorage = await this.storageRepository.getListStorage(data, userId);
        if (listStorage.length) {
            const result = this._formatResultGetList(listStorage);
            return {
                isSuccess: true,
                data: result
            }
        }
        return {
            isSuccess: true,
            data: []
        }
    }

    private _formatResultGetList(listStorage: StorageEntity[]): any {
        let listClone: any = listStorage.sort((a, b) => a.path.length - b.path.length).map((e) => {
            const result: any = { ...e };
            if (e.type === StorageTypeEnum.FOLDER) {
                result.listChildrens = []
            }
            result.level = e.path.match(/\/[^\/]+/g)?.length || 0;
            return result
        })
        let rootElement = listClone[0];
        let listElementWithLevel = [rootElement]
        for (let i = rootElement.level + 1; i <= listClone[listClone.length - 1].level; i++) {
            const listStorageWithLevel = listClone.filter(e => e.level === i);
            listStorageWithLevel.forEach(storage => {
                const targetElement = listElementWithLevel.find((element) => {
                    return storage.path.includes(element.path)
                });
                targetElement.listChildrens.push(storage)
            })
            listElementWithLevel = listStorageWithLevel;
        }
        return rootElement;
    }

    public async changeDirectory(path: string, userId: number): Promise<Checker> {
        const folder = await this.storageRepository.findFolderWithPath(path, userId);
        if (folder) {
            return {
                isSuccess: true,
                data: null
            }
        }
        return Constants.FAIL_CHECK
    }

    public async removeStorage(data: RemoveStorageDTO, userId: number): Promise<Checker> {
        const result = await this.storageRepository.removeStorage(data, userId);
        return {
            isSuccess: result,
            data: null
        }
    }

    public async moveStorage(data: MoveStorage, userId: number): Promise<Checker> {
        const desArr = data.des.split("/");
        const parentPath = desArr.slice(0, desArr.length - 1).join("/") || "/";
        const [parentEntity, desEntity] = await Promise.all([
            this.storageRepository.findFolderWithPath(parentPath, userId),
            this.storageRepository.findFolderWithPath(parentPath, userId, false)
        ])
        if (parentEntity) {
            if (data.source.length > 1) { // more than one source
                if (!desEntity || desEntity.type === StorageTypeEnum.FILE) {
                    return Constants.FAIL_CHECK;
                }
            }
            await Promise.allSettled(data.source.map((e: string) => this._moveStorage(e, data.des, userId)));
            return {
                isSuccess: true,
                data: null
            }
        }
        else {
            return Constants.FAIL_CHECK;
        }
    }

    private async _moveStorage(pathSrc: string, pathDes: string, userId: number): Promise<boolean> {
        // Get list source
        const listStorage = await this.storageRepository.getListStorageWithPath(pathSrc, userId);
        if (listStorage.length) {
            let listClone = listStorage.sort((a, b) => a.path.length - b.path.length);
            const firstElementMatch = listClone[0];
            const pathLengthRemove = firstElementMatch.path.split(Constants.ROOT_PATH).length - 1;
            // Delete
            const canDelete = await this.storageRepository.removeStorage({
                isRecursive: true,
                path: pathSrc
            }, userId);
            if (canDelete) { // Create
                await Promise.allSettled([
                    listStorage.map(e => {
                        const pathArr = e.path.split("/");
                        const pathAdd = pathArr.slice(pathLengthRemove, pathArr.length);
                        const newPath = pathDes + Constants.ROOT_PATH + pathAdd.join("/");
                        return this.createNewFile({
                            data: e.data,
                            isCreate: true,
                            path: newPath
                        }, userId)
                    })
                ]);
            }
        }
        return false;
    }
}
