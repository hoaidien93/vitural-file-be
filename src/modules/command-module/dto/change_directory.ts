import { IsNotEmpty, IsString } from 'class-validator';

export class ChangeDirectoryDTO {
    @IsString()
    @IsNotEmpty()
    path: string;
}
