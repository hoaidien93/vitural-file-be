import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class RemoveStorageDTO {
    @IsString()
    @IsNotEmpty()
    path: string;

    @IsBoolean()
    @IsNotEmpty()
    isRecursive: boolean;
}
