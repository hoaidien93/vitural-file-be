import { IsBoolean, IsNotEmpty, IsNumber, IsString, ValidateIf } from 'class-validator';

export class CreateFileDTO {
    @IsString()
    @IsNotEmpty()
    path: string;

    @IsString()
    @IsNotEmpty()
    @ValidateIf((object, value) => value !== null)
    data: string | null;

    @IsBoolean()
    @IsNotEmpty()
    isCreate: boolean;
}
