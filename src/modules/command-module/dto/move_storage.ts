import { IsArray, IsBoolean, IsNotEmpty, IsNumber, IsString, Min, ValidateIf } from 'class-validator';

export class MoveStorage {
    @IsArray()
    @IsNotEmpty()
    source: string[];

    @IsString()
    @IsNotEmpty()
    des: string;
}
