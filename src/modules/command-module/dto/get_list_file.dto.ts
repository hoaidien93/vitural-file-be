import { IsBoolean, IsNotEmpty, IsNumber, IsString, Max, Min } from 'class-validator';

export class GetListFileDTO {
    @IsString()
    @IsNotEmpty()
    path: string;

    @IsBoolean()
    @IsNotEmpty()
    isLongList: boolean;

    @IsNumber()
    @IsNotEmpty()
    @Min(1)
    @Max(20)
    level: number;
}
