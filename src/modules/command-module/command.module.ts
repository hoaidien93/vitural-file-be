import { Module } from '@nestjs/common';
import { StorageModule } from '../storage-module/storage.module';
import { CommandController } from './command.controller';
import { CommandService } from './command.service';

@Module({
    imports: [
        StorageModule
    ],
    controllers: [CommandController],
    providers: [CommandService],
    exports: []
})
export class CommandModule { }
