import { Body, Controller, Post, Req } from '@nestjs/common';
import { JSONResponse } from 'src/interfaces/response.interface';
import { handleChecker } from 'src/utils/helpers';
import { CommandService } from './command.service';
import { CreateFileDTO } from './dto/create_file.dto';
import { Request } from 'express';
import { GetListFileDTO } from './dto/get_list_file.dto';
import { ChangeDirectoryDTO } from './dto/change_directory';
import { RemoveStorageDTO } from './dto/remove_storage.dto';
import { MoveStorage } from './dto/move_storage';

@Controller("/command")
export class CommandController {
    constructor(
        private readonly commandService: CommandService
    ) { }

    @Post("create-new-file")
    async createNewFile(@Body() data: CreateFileDTO, @Req() req: Request): Promise<JSONResponse> {
        const result = await this.commandService.createNewFile(data, req.user.id);
        return handleChecker(result);
    }

    @Post("get-list-file")
    async getListFile(@Body() data: GetListFileDTO, @Req() req: Request): Promise<JSONResponse> {
        const result = await this.commandService.getListFile(data, req.user.id);
        return handleChecker(result)
    }

    @Post("change-directory")
    async changeDirectory(@Body() data: ChangeDirectoryDTO, @Req() req: Request): Promise<JSONResponse> {
        const result = await this.commandService.changeDirectory(data.path, req.user.id);
        return handleChecker(result)
    }

    @Post("remove-storage")
    async removeStorage(@Body() data: RemoveStorageDTO, @Req() req: Request): Promise<JSONResponse>{
        const result = await this.commandService.removeStorage(data, req.user.id);
        return handleChecker(result)
    }

    @Post("move-storage")
    async moveStorage(@Body() data: MoveStorage, @Req() req: Request): Promise<JSONResponse>{
        const result = await this.commandService.moveStorage(data, req.user.id);
        return handleChecker(result)
    }
}
