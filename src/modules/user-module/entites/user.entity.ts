import { StorageEntity } from 'src/modules/storage-module/entities/storage.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity({ name: 'users' })
@Unique("unq_user_email", ["email"])
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column()
    created_at: Date;

    @Column()
    updated_at: Date;

    @OneToMany(type => StorageEntity, storage => storage.owner)
    listStorage: StorageEntity[]
}
