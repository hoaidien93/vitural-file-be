import { Body, Controller, Post } from '@nestjs/common';
import { JSONResponse } from 'src/interfaces/response.interface';
import { handleChecker } from 'src/utils/helpers';
import { LoginDTO } from './dto/login.dto';
import { RegisterDTO } from './dto/register.dto';
import { UserService } from './user.service';

@Controller("/user")
export class UserController {
    constructor(private readonly userService: UserService) { }

    @Post("register")
    async postRegister(@Body() data: RegisterDTO): Promise<JSONResponse>{
        const result = await this.userService.register(data);
        return handleChecker(result);
    }

    @Post("login")
    async postLogin(@Body() data: LoginDTO): Promise<JSONResponse>{
        const result = await this.userService.login(data);
        return handleChecker(result);
    }
}
