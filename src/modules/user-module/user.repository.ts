import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegisterDTO } from './dto/register.dto';
import { UserEntity } from './entites/user.entity';


@Injectable()
export class UserRepository {
    constructor(
        @InjectRepository(UserEntity)
        private readonly userRepository: Repository<UserEntity>,
    ) { }

    async register(data: RegisterDTO): Promise<UserEntity> {
        const newUser = this.userRepository.create({
            email: data.email,
            password: data.password
        });
        try {
            const result = await this.userRepository.save(newUser);
            return result;
        } catch (e) {
            return null;
        }
    }

    async findUserByEmail(email: string): Promise<UserEntity> {
        return await this.userRepository.findOne({ email })
    }
}