import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './modules/user-module/user.module';
import { UserData } from './interfaces/user_data.interface';
import { AuthMiddleware } from './middlewares/auth.middleware';
import { CommandModule } from './modules/command-module/command.module';

declare module "express" {
  export interface Request {
    user: UserData
  }
}


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '54.179.70.143',
      port: 3306,
      username: 'root',
      password: '1',
      database: 'virtual_file',
      synchronize: true,
      entities: ["dist/**/*.entity{.ts,.js}"],
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: ".env"
    }),
    UserModule,
    CommandModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consume: MiddlewareConsumer) {
    consume.apply(AuthMiddleware).exclude(
      "user/login",
      "user/register",
    ).forRoutes("*")
  }
}
